//
//  MaterialDetailsViewController.swift
//  PBRforPLA
//
//  Created by Yulibar Husni on 21/10/20.
//

import UIKit
import SceneKit

class MaterialDetailsViewController: UIViewController {
    
    var materialDetails: String?
    var materialTitle: String?
    var scene: SCNScene?
    @IBOutlet weak var scenekitViewer: SCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = materialTitle
        sceneSetup()
    }
    
    func sceneSetup() {
        // Select the Scene file.
        scene = SCNScene(named: "art.scnassets/Scene/\(materialTitle ?? "Default").scn")
        scenekitViewer.allowsCameraControl = true
        // Set the initial environment map
        var environment: UIImage?
        environment = UIImage(named: "art.scnassets/environments/Day.jpg")
        scene!.lightingEnvironment.contents = environment
        scene!.background.contents = environment
        // Additional Scene setup
        scenekitViewer.preferredFramesPerSecond = 120 // for iPad Pro, SceneKit can run with 120FPS maximum.
        scenekitViewer.showsStatistics = true // SceneKit Statistics
        scenekitViewer.scene = scene // Set the Scene to be shown at the viewer
    }
    
    @IBAction func dayNightSC(_ sender: UISegmentedControl) {
        var environment: UIImage?
        switch sender.selectedSegmentIndex {
        case 0:
            environment = UIImage(named: "art.scnassets/environments/Day.jpg")
        case 1:
            environment = UIImage(named: "art.scnassets/environments/Dusk.jpg")
        default:
            environment = UIImage(named: "art.scnassets/environments/Night.jpg")
        }
        scene!.lightingEnvironment.contents = environment
        scene!.background.contents = environment
    }
    
}

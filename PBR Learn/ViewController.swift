//
//  ViewController.swift
//  PBRforPLA
//
//  Created by Yulibar Husni on 25/09/20.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableMaterials: UITableView!
    var materials: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        scan()
        tableMaterials.delegate = self
        tableMaterials.dataSource = self
        tableMaterials.register(UITableViewCell.self, forCellReuseIdentifier: "materials")

    }
    
    // MARK: Get the Scene name from Scene folder to be added to the materials array.
    func scan() {
        let fm = FileManager.default
        let path = Bundle.main.resourcePath!
        do {
            let items = try fm.contentsOfDirectory(atPath: "\(path)/art.scnassets/Scene")

            for item in items {
                // remove the last 4 character from the string ( . s c n )
                materials.append(String(item.dropLast(4)))
            }
        } catch {
        }
    }
}

    // MARK: TableView setup
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return materials.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "materials", for: indexPath)
        cell.textLabel?.text = materials[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "scenekit-viewer") as! MaterialDetailsViewController
        vc.materialTitle = materials[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}
